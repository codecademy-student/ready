$(document).ready(function() {
    MatchGame.setup();

    $('button').click(function() {
	MatchGame.setup();
    });

});

var MatchGame = {};

/*
  Setup the Game
 */

MatchGame.setup = function () {
    MatchGame.renderCards(MatchGame.generateCardValues(), '.game .row');
    score.updateLowScore();
    score.total = 0;
    score.gameOver = false;
    $('.score').html(score.total);
};

/*
  Generates and returns an array of matching card values.
 */

MatchGame.generateCardValues = function () {

    var cards = [];
    var randomCards = [];
    
    for (var i = 1; i < 9; i++) {
	cards.push(i);
	cards.push(i);
    }

    while (cards.length > 0) {
	var randomIndex = Math.floor(Math.random() * (cards.length - 0));
	randomCards.push(cards[randomIndex]);
	cards.splice(randomIndex, 1);
    }
    
    return randomCards;
};

/*
  Converts card values to jQuery card objects and adds them to the supplied game
  object.
*/

MatchGame.renderCards = function(cardValues, $game) {

    $($game).data({
	"flippedCards": [],
	"matchedCards": []
    });
    
    var cardColors = [
	"hsl(25, 85%, 65%",
	"hsl(55, 85%, 65%",
	"hsl(90, 85%, 65%",
	"hsl(160, 85%, 65%",
	"hsl(220, 85%, 65%",
	"hsl(265, 85%, 65%",
	"hsl(310, 85%, 65%",
	"hsl(360, 85%, 65%"
    ];
    
    $($game).empty();
    
    for (i=0; i < cardValues.length; i++) {
	var $card = $('<div class="col-xs-3 card"></div>');
	$card.data({
	    "value": cardValues[i],
	    "flipped": false,
	    "color": cardColors[cardValues[i] - 1]
	});

	$($game).append($card);
    }

    $('.card').click(function() {
	MatchGame.flipCard($(this), $($game));
    });
    
};

/*
  Flips over a given card and checks to see if two cards are flipped over.
  Updates styles on flipped cards depending whether they are a match or not.
 */

MatchGame.flipCard = function($card, $game) {

    var $card = $($card);
    var $game = $($game);
    var $cardValue = $card.data("value");
    var $flippedCards = $game.data("flippedCards");
    var $matchedCards = $game.data("matchedCards");

    function unflipCard($card) {
	$($card).html('');
	$($card).css('background-color', 'rgb(32, 64, 86)');
	$($card).data("flipped", false);
    };

    // Flip a card
    if ($card.data("flipped")) {
	// card already flipped
	return;
    } else {
	// flip the card
	$card.html($cardValue);
	$card.css('background-color', $card.data("color"));
	$card.data("flipped", true);
	score.incrementScore();
	$('.score').html(score.total);

	if ($flippedCards.length !== 0) {
	    // 2nd card is flipped

	    var $card2 = $flippedCards[0];

	    if ($cardValue === $card2.data("value")) {
		// cards match
		$card.css('background-color', 'rgb(153, 153, 153)');
		$card2.css('background-color', 'rgb(153, 153, 153)');
		$game.data("flippedCards", []);
		$card.data("flipped", true)
		$card2.data("flipped", true)
		$matchedCards.push($card)
		$matchedCards.push($card2)
		score.checkGameOver($game);
	    } else {
		// cards don't match
		window.setTimeout(function() {
		    unflipCard($card);
		    unflipCard($card2);
		}, 500);
		$game.data("flippedCards", []);
	    }

	} else {
	    $flippedCards.push($card);
	}
    }    
};


// Calculate score
var score = {
    total: 0,
    gameOver: false,
    lowScore: 1000,

    incrementScore: function () {
	score.total++;
    },
    
    updateLowScore: function() {
	if (score.lowScore === 1000) {
	    $('.low-score').html(0);
	}

	if (score.gameOver === true) {
	    if (score.total < score.lowScore) {
		score.lowScore = score.total;
	    }
	    $('.low-score').html(score.lowScore);
	}
    },

    checkGameOver: function($game) {
	if ($($game).data("matchedCards").length === 16) {
	    score.gameOver = true;
	    score.updateLowScore();
	}
    }

}
